#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <unistd.h>
using namespace std;

#include "checkGPU.h"
#include <omp.h>
#include <mpi.h>



/*////////////
 *
 * Simple master-Worker pattern by MPI+OpenMP:
 *   Require one MPI process for each physical node: 
 *   spawing GPU+1 thread on rank-0 (1 as the master) and GPU threads on others.
 *   One thread for each worker/GPU.
 */


//const char FILE_PARAM[] = "pars";

enum DET {HLVK=0, HLV, HLK, HVK, LVK};
const char* int2det(int i)
{
    static const char *str[] = { "HLVK", "HLV", "HLK", "HVK", "LVK"};
    return str[i];
}
static inline int det2int(const char* det)
{
    int i = -1;
    if      ( strcmp(det, "HLVK") == 0 )   i=0;
    else if ( strcmp(det, "HLV" ) == 0)   i=1;
    else if ( strcmp(det, "HLK" ) == 0)   i=2;
    else if ( strcmp(det, "HVK" ) == 0)   i=3;
    else if ( strcmp(det, "LVK" ) == 0)   i=4;
    
    return i;
}
inline int uniq_tag(int nt, int rank) { return rank*1000 + nt;  };
int ROOT_NODE = 0;

class data {
    
    ifstream f;
    int  eid;
    string det;
    
    public:
    data(const char* fname)  {
       f.open(fname, ios::in);
       if (!f.is_open())   {
	 cout << "Cacnnot open input file."<< endl;
         //MPI_Finalize();
	 exit(0);
       }
       cout << "Load parameter file : " << fname << endl;
    }
    ~data() {
        f.close();
    }
    
    bool next() {
	if (!f.eof()) {
	    f >> eid >> det; 
	    //printf("%d --- %s\n", eid, det);
	    return true;
	} else 
	    return false;
    }
    int   get_eid() { return eid; }
    string get_det() { 
	return det;
    }
};


void lanuch_provider(int n_worker, const char* parfile) {
    
    printf("Lanuch master for %d workers.\n", n_worker);

    int buf[2], len, ierr;
    MPI_Status status;
    char str[512];

    int count= 0, done=0;
    data par(parfile);
    while(1) {

	ierr = MPI_Recv(buf, 2, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	if (ierr != MPI_SUCCESS) {
	    MPI_Error_string(ierr, str, &len);
	    printf("** Master recv exception : %s !\n", str);
	}
	
	int to_source = status.MPI_SOURCE;
	int to_tag    = status.MPI_TAG;
	//printf("Gettng worker %d - tag %d \n",  to_source, to_tag );

	if (!par.next()) {
	    count ++;     // One more worker stopped.
	    buf[0] = -1;  // Send -1 to stop this worker
	    buf[1] = 0;
	} else {
	    buf[0] =         par.get_eid();
	    buf[1] = det2int(par.get_det().c_str());
	}

	ierr = MPI_Send(buf, 2, MPI_INT, to_source, to_tag, MPI_COMM_WORLD); 
	if ( ierr != MPI_SUCCESS  )  {
	    printf("** Master send exception !\n");
	} else {
	    //printf("Sending (%d,%d) to worker %d tag %d \n", buf[0], buf[1],  to_source, to_tag );
	}

	if (0) {
	   done++;
	   if (done%200==0)  system("nvidia-smi");
	}
	
	if (count >= n_worker) break;  // Stop the master if all worker were stopped.
    }
    printf("[Done] No more worker - Master exit.\n");
};


int lanuch_worker_for_gpu_id(int nt, int rank) {

    printf("Lanuch worker %d-%d ...\n", rank, nt);

    //
    MPI_Status status;
    int bsend[2], brecv[2], len, ierr, tag;
    char str[512];
    
    while(1) {
    
	tag = uniq_tag(nt, rank);
        //ierr = MPI_Sendrecv(bsend, 2, MPI_INT, ROOT_NODE, tag, brecv, 2, MPI_INT, ROOT_NODE, tag, MPI_COMM_WORLD, &status);
	MPI_Send(bsend, 2, MPI_INT, ROOT_NODE, tag, MPI_COMM_WORLD);
	ierr = MPI_Recv(brecv, 2, MPI_INT, ROOT_NODE, tag, MPI_COMM_WORLD, &status);
	if (ierr != MPI_SUCCESS) {
	    //MPI_Error_class(ierr,&errclass);
	    MPI_Error_string(ierr, str, &len);
	    printf("** Worker %d-%d fail : %s\n", rank, nt, str);
	    continue;
	}
    
        int         eid = brecv[0];
        const char* det = int2det(brecv[1]);
    
        //printf("Process (%d, %s) on worker %d-%d.\n",eid, det, rank,nt);

        if (eid < 0) break; // No more jobs
        
        sprintf(str, "(export GPU_ID=%d; sh/simx.sh %d %s)", nt, eid, det);
        //cout<< str << endl;
        #if 1
        system(str);
        #endif
        
    }
    
    printf("No jobs from master - worker %d-%d exit.\n", rank,nt);
    return 0;
}


int main(int argc, char *argv[])  {

    if (argc != 2) {
	printf("Usage: dist [input pars]\n");
	exit(0);
    }
    const char* input_pars = argv[1];
    
    int rank, size, provided;
    //MPI_Init(&argc, &argv);
    MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);    // MPI_THREAD_SERIALIZED
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // detect number of GPU on each node (can be different)
    int NGPU = checkGPU();
     
    if (rank==0) {
	
        #pragma omp parallel num_threads(NGPU+1)
        {
    	   int nt = omp_get_thread_num();
	   if (nt == NGPU) {
		lanuch_provider((size)*NGPU, input_pars);
	   }
	   else {
	   	lanuch_worker_for_gpu_id(nt, rank);
	    }
	}
    } else {
        //omp_set_num_threads(NGPU);
        #pragma omp parallel num_threads(NGPU)
        {
    	    int nt = omp_get_thread_num();
	    lanuch_worker_for_gpu_id(nt, rank);
	}   
    }
    
    //MPI_Barrier(MPI_COMM_WORLD);
    
    MPI_Finalize();
    return 0;
}
