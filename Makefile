include Makefile.gpu

TARGET  = dist
OBJECTS = dist.o checkGPU.o

###########################################################
all: $(TARGET)

$(TARGET):$(OBJECTS)
	$(LD) $(OBJECTS) -o $@ $(LIBS)

clean:
	rm $(OBJECTS) $(TARGET) -f
##########################################################
